declare module '*.css' {
  export const css: Record<any, any>;
  export default css;
}

declare module '*.scss' {
  const scss: Record<any, any>;
  export default scss;
}
declare module '*.svg' {
  import React from 'react';

  export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
}
