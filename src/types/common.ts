export type ClassName = { className?: string };

export type Callback<R = any, Args extends any[] = any[]> = (
  ...args: Args
) => R;

export type TabsType = Record<string, number>;
