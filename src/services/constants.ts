import { TabsType } from '../types/common';

export const libraryTabs: TabsType = {
  stories: 0,
  games: 1,
  songs: 2,
  printables: 3,
  goGoCam: 4,
  choiceBoard: 5,
};
