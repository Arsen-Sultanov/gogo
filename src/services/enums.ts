export enum NAVIGATION_BUTTONS {
  Stories = 'stories',
  Games = 'games',
  Songs = 'songs',
  Printables = 'printables',
  GoGoCam = 'goGoCam',
  ChoiceBoard = 'choiceBoard',
}
