export const customNavigationButtonsColors = {
  stories: '#33ACFE',
  games: '#33D291',
  songs: '#EFD81C',
  printables: '#EB1268',
  goGoCam: '#F59C07',
  choiceBoard: '#902CFF',
};
