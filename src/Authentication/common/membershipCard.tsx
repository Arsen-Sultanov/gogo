import { Button, Card, Row } from "react-bootstrap";

type BillingMode = 'monthly' | 'annually'

const billingModeDisplayMap = {
    'monthly': 'month',
    'annually': 'year'
}

export type MembershipCardProps = {

    title: string,
    price: number,
    freeTrialTTL: number,
    accessList: string[],
    billingMode: BillingMode,
    handleClick: (event: any) => void,

    savingPercentage?: number,
    className?: string
}


const MembershipCard = (props: MembershipCardProps) => {
    const { title, price, freeTrialTTL, accessList, billingMode,
                handleClick, savingPercentage, className } = props;

    const classes = className ? `${className} membership-card-container` : 'membership-card-container';

    const renderMembershipAccessList = () => {
        return accessList.map((access, index) => {
            return <Row><span className="ps-4 color--tertiary font-family-serious" key={index}>{index+1}: {access}</span></Row>
        })
    }

    const renderMembershipCardHeader = () => <>
        <Row>
            <span className={'color--tertiary membership-card-title'}>{title}
                {billingMode &&
                    <span className={'color--tertiary membership-card-title'}> - {billingMode.toUpperCase()}</span>} 
                {savingPercentage &&
                    <span className={'color--tertiary membership-card-title'}> (Save up to {savingPercentage}%)</span>} 
            </span>        
        </Row>
        <Row className={'justify-content-center'}>
            <span className={'color--primary membership-card-price'}>
                ${price}
                <span className={'color--black font-family-serious fs-6'}> / {billingModeDisplayMap[billingMode]} </span>
            </span>
        </Row>
        <Row><span className="justify-content-center d-flex color--dark-gray">Billed {billingMode}. Cancel anytime.</span></Row>
        <Row><div className="justify-content-center color--primary mt-2 d-flex">
                <span className='membership-card-trial'>{freeTrialTTL} Free Trial</span>
            </div>
        </Row>
    </>

    const renderMembershipCardBody = () => <>
        <Row ><span className={'color--tertiary font-family-serious'}>Full Access to the GoGo Speech Library:</span></Row>
        {renderMembershipAccessList()}
    </>

    const base = <Card className={classes}>
            <Card.Body className="p-3">
                {renderMembershipCardHeader()}
            </Card.Body>
            <div className="membership-card-border p-0"></div>
            <Card.Body className="p-3">
                {renderMembershipCardBody()}
                <Row className='mt-3 me-2'>
                    <Button type='button' onClick={handleClick}
                        className={'custom-text-stroke-tertiary-9 custom-text-letter-spacing-negative-12 custom-box-shadows-none button-primary text-center fw-bold'}>
                        <span className='fw-boldest font-size-7 login-text'>Start Trial</span>
                    </Button> 
            </Row>
            </Card.Body>
        </Card>
    
    return base;
            
}

export default MembershipCard;
