import { Form } from "formik"
import { Card, Row } from "react-bootstrap"
import { renderFormikField, renderFormikForm, renderSubmitButton } from "../../Utils/FormHelpers/formikHelpers"
import { renderBaseContainer, renderLinks, renderLogo } from "../../Utils/uiHelpers"
import { formikValidationBuilder } from "../../Utils/FormHelpers/formikValidationBuilder"

const Login = (props: any) => {
    
    const validation = formikValidationBuilder([
        {
            name: 'email',
            errorMessageMap: {
                email: 'Email must be valid',
                required: 'Email is required',
            },
            validationModes: ['required', 'email']
        },
        {
            name: 'password',
            errorMessageMap: {
                required: 'Password is required',
            },
            validationModes: ['required']
        }
    ])

    const initialValues = {
        email: "",
        password: ""
    }

    const handleOnSubmit = async (formValues: {}): Promise<void> => {

    }

    const renderForm = () => <Form>
        <Row className='mt-2'>{renderFormikField("email", "Email", "Enter Email")}</Row>
        <Row className='mt-2'>{renderFormikField("password", "Password", "Enter Password", 'password')}</Row>
        {renderSubmitButton("Log in")}
    </Form>

    return <>
        {renderLogo()}
        <div className={'Login--container d-flex justify-content-center'}>
            <Card className='custom-card-container' >
                <Card.Body>
                    {renderBaseContainer(renderFormikForm(validation, initialValues, handleOnSubmit, renderForm), 
                            'Log in')}
                    {renderLinks('Don\'t have an account?', ' Sign Up', '/register')}
                    {renderLinks('Forgot password?', ' Reset password', '')}
                </Card.Body>
            </Card> 
        </div>
    </>
    

}

export default Login