import _ from "lodash";
import { useEffect, useState } from "react";
import MembershipCard from "../common/membershipCard";
import Membership from "./membership";
import PaymentDetails from "./paymentDetails";
import Register from "./register";
import VerifyEmail from "./verifyEmail";

type RegistrationSteps = 'register' | 'verify' | 'membership' | 'paymentDetails';

const RegistrationWrapper = (props: any) => {

    const [currentStep, setCurrentStep] = useState('paymentDetails');

    const sharedProps = {
        currentStep,
        setCurrentStep,
    }

    const registrationSteps: {[key in RegistrationSteps]?: JSX.Element} = {
        register: <Register {...sharedProps} />,
        verify: <VerifyEmail {...sharedProps} />,
        membership: <Membership {...sharedProps}/>,
        paymentDetails: <PaymentDetails  {...sharedProps}></PaymentDetails>,
    }

    return _.get(registrationSteps, currentStep);

}

export default RegistrationWrapper;

