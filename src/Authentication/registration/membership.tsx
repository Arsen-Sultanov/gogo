import React from "react";
import { Button, Col, Row } from "react-bootstrap";
import ToggleButton from "../../GenericUI/ToggleButton";
import { renderBaseContainer } from "../../Utils/uiHelpers";
import MembershipCard from "../common/membershipCard";

type PricingMode = 'single' | 'group'

const Membership = (props: {setCurrentStep: Function, currentStep: string}) => {

    const { currentStep, setCurrentStep } = props;

    const [isSingleUser, setIsSingleUser] = React.useState(true);

    const pricingMode = isSingleUser === true ? 'single': 'group';

    const togglePricingMode = <Row className='pb-4 d-flex justify-content-center'>
            <ToggleButton leftText="Single User" rightText="Group" 
                leftTextClassName="custom-text-stroke-tertiary-9 custom-border-solid-tertiary-10 font-size-6 custom-text-letter-spacing-negative-12" 
                rightTextClassName="custom-text-stroke-tertiary-9 custom-border-solid-tertiary-10 font-size-6 custom-text-letter-spacing-negative-12"
                selected={isSingleUser} toggleSelected={(event) => setIsSingleUser(!isSingleUser)}></ToggleButton>
        </Row>

    const singleUserCards = <Row>
        <Col className='mx-5'><MembershipCard title={'SINGLE USER'} billingMode="monthly" price={16} freeTrialTTL={7} className='membership-card-width'
            accessList={['Stories', 'Games', 'Songs', 'Printables', 'GoGo Cam', 'and more!']} handleClick={(event) => setCurrentStep('register')} /></Col>
        <Col className='mx-5'><MembershipCard title={'SINGLE USER'} billingMode="annually" price={144} freeTrialTTL={7} className='membership-card-width'
            savingPercentage={25} accessList={['Stories', 'Games', 'Songs', 'Printables', 'GoGo Cam', 'and more!']} handleClick={(event) => setCurrentStep('register')} /></Col>
    </Row>
        
        

    const groupUserCards =  <Row className="pb-2">
        <Col className='mx-5'><MembershipCard title={'SINGLE USER'} billingMode="monthly" price={16} freeTrialTTL={7} className='membership-card-width'
            accessList={['Stories', 'Games', 'Songs', 'Printables', 'GoGo Cam', 'and more!']} handleClick={(event) => setCurrentStep('register')} /></Col>
    </Row>
    
    const render = <Row className="pb-2">
        {togglePricingMode}
        {isSingleUser === true ? singleUserCards : groupUserCards}
        <Row><h3 className={'text-center color--quaternary mt-5'}>Purchase Orders</h3></Row>
    </Row>

    return renderBaseContainer(render, 'Plans & Pricing')

}

export default Membership;
