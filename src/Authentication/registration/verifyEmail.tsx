import { Card, Container, Row } from "react-bootstrap"
import { renderBaseContainer, renderLinks, renderLogo } from "../../Utils/uiHelpers"
import MembershipCard from "../common/membershipCard"


const VerifyEmail = (props: {setCurrentStep: Function, currentStep: string}) => {

    return <> 
        {renderLogo()}
        <div className={'Login--container d-flex justify-content-center'}>
            <Card className='custom-card-container'>
                <Card.Body>
                    {renderBaseContainer(<></>, 'Verify Your Email')}
                    {renderLinks('No email in your inbox or spam folder', ' resend code', '/')}
                </Card.Body>
            </Card> 
        </div>

    </>

}


export default VerifyEmail
