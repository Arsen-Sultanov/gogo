import { Form } from "formik"
import React, { useEffect } from "react"
import { Card, Col, Row } from "react-bootstrap"
import '../../assets/logo.png'
import { renderFormikField, renderFormikForm, renderSubmitButton } from "../../Utils/FormHelpers/formikHelpers"
import { renderBaseContainer, renderLinks, renderLogo } from "../../Utils/uiHelpers"
import { formikValidationBuilder } from "../../Utils/FormHelpers/formikValidationBuilder"

const Register = (props: {setCurrentStep: Function, currentStep: string}) => {

    const [validation, setValidation] = React.useState(undefined)

    useEffect(() => {
        if (!validation) {
            const newValidation = formikValidationBuilder([
                {
                    name: 'first_name',
                    errorMessageMap: {
                        'required': 'First name is required'
                    },
                    validationModes: ['required']
                },
                {
                    name: 'last_name',
                    errorMessageMap: {
                        'required': 'Last name is required'
                    },
                    validationModes: ['required']
                },
                {
                    name: 'email',
                    errorMessageMap: {
                        'required': 'Email is required',
                        'email': 'Email is invalid'
                    },
                    validationModes: ['required', 'email']
                },
                {
                    name: 'password',
                    errorMessageMap: {
                        'required': 'Password is required',
                        'min': 'Password must be at least 8 characters'
                    },
                    validationModes: ['required', 'min'],
                    additionalConfig: {
                        'min': 8
                    }
                }
            ])
            setValidation(newValidation as any)
        }
    }, []);

    const initialValues = {
        first_name: "",
        last_name: "",
        email: "",
        password: ""
    }

    const handleOnSubmit = async (formValues: {}): Promise<void> => {
        props.setCurrentStep('verify')
    }

    const renderForm = () => <Form>
       <Row className="mt-2">
            <Col className={'ps-0'} xl="6" sm ="6"lg="6" xs="6" xxl="6" md="6">
                {renderFormikField("first_name", "First Name*", "Enter First Name")}
            </Col>
            <Col className={'pe-0'} xl="6" sm ="6"lg="6" xs="6" xxl="6" md="6">
                {renderFormikField("last_name", "Last Name*", "Enter Last Name")}
            </Col> 
        </Row> 
        <Row className="mt-2">{renderFormikField("email", "Email", "Enter Email")}</Row> 
        <Row className="mt-2">{renderFormikField("password", "Password", "Enter Password", "password")}</Row>
        <Row className="mt-2">{renderSubmitButton("Continue")}</Row>
    </Form>

    if (!validation) {
        return <></>
    }

    return <>
        {renderLogo()}
        <div className={'Login--container d-flex justify-content-center'}>
            <Card className='custom-card-container'>
                <Card.Body>
                    {renderBaseContainer(renderFormikForm(validation, initialValues, handleOnSubmit, renderForm), 
                            'Create Your Account')}
                    {renderLinks('Already have an account?', ' Log in', '/')}
                </Card.Body>
            </Card> 
        </div>
    </>

}

export default Register
