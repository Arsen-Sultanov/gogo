import React from 'react';
import { Link } from 'react-router-dom';
import SubLogo from '../SubLogo';
import { ReactComponent as Instagram } from '../../assets/icons/inst.svg';
import { ReactComponent as Facebook } from '../../assets/icons/facebook.svg';
import { ReactComponent as Youtube } from '../../assets/icons/youtube.svg';
import style from './style.module.scss';

const FOOTER_NAV_ITEMS = [
  {
    label: 'How to GoGo',
    to: '/howToGoGo',
  },
  {
    label: 'Pricing',
    to: '/pricing',
  },
  {
    label: 'Group Quote',
    to: '/groupQuote',
  },
  {
    label: 'FAQ',
    to: '/faq',
  },
  {
    label: 'Contact',
    to: '/contact',
  },
];

const TERMS = [
  {
    label: '© 2021 GoGo Speech, LLC',
    to: '/terms',
  },
  {
    label: 'Terms',
    to: '/terms',
  },
  {
    label: 'Privacy',
    to: '/privacy',
  },
];

const SOCIALS = [
  {
    icon: <Instagram />,
    to: '/instagram',
  },
  {
    icon: <Youtube />,
    to: '/youtube',
  },
  {
    icon: <Facebook />,
    to: '/facebook',
  },
];

const Footer = () => (
  <footer className={style.footer}>
    <div className={style.subLogoWrapTablet}>
      <SubLogo />
    </div>
    <nav className={style.footerNav}>
      <ul className={style.footerNavList}>
        {FOOTER_NAV_ITEMS.map((footerNavItem) => (
          <li className={style.footerNavItem}>
            <Link to={footerNavItem.to} className={style.link}>
              {footerNavItem.label}
            </Link>
          </li>
        ))}
      </ul>
      <ul className={style.footerTerms}>
        {TERMS.map((footerTermsItem) => (
          <li className={style.footerTermsItem}>
            <Link to={footerTermsItem.to} className={style.link}>
              {footerTermsItem.label}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
    <div className={style.socialsWrap}>
      <ul className={style.socials}>
        {SOCIALS.map((socialItem) => (
          <li className={style.socialItem}>
            <Link to={socialItem.to} className={style.socialItemLink}>
              {socialItem.icon}
            </Link>
          </li>
        ))}
      </ul>
      <div className={style.subLogoWrap}>
        <SubLogo />
      </div>
    </div>
  </footer>
);

export default Footer;
