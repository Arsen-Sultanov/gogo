import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as ArrowIcon } from '../../assets/icons/arrow.svg';

import style from './style.module.scss';

const ArrowButton = ({ className }: { className: string }) => (
  <Link to="/library" className={className}>
    <ArrowIcon className={style.arrowIcon} />
  </Link>
);

export default ArrowButton;
