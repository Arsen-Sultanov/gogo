import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as SearchIcon } from '../../../assets/icons/search.svg';
import style from './style.module.scss';

const Search = () => (
  <Link to="/#" className={style.search}>
    <SearchIcon className={style.searchIcon} />
  </Link>
);

export default Search;
