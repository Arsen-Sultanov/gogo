import React from 'react';
import { ReactComponent as BurgerIcon } from '../../../assets/icons/burger.svg';
import style from './style.module.scss';

const Burger = () => (
  <button type="button" className={style.burger}>
    <BurgerIcon className={style.burgerIcon} />
  </button>
);

export default Burger;
