import React from 'react';
import style from './style.module.scss';

const GeneralButton = ({ children }) => (
  <button type="button" className={style.generalButton}>
    <p className={style.generalButtonTitle}>{children}</p>
  </button>
);

export default GeneralButton;
