import React from 'react';
import Carousel from 'react-multi-carousel';
import Arrow from './Arrow';
import Card from './Card';
import style from './style.module.scss';
import 'react-multi-carousel/lib/styles.css';

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 2000, min: 3000 },
    items: 4,
    partialVisibilityGutter: 150,
    slidesToSlide: 4,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
    partialVisibilityGutter: 150,
    slidesToSlide: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 500 },
    items: 2,
    slidesToSlide: 2,
  },
  mobile: {
    breakpoint: { max: 500, min: 0 },
    items: 1,
    slidesToSlide: 1,
  },
};

type Props = {
  title: string;
  data: number[];
  titleColor?: string;
  arrowColor?: string;
};

const CarouselWithTitle = ({ title, data, titleColor, arrowColor }: Props) => (
  <div>
    <p className={style.carouselTitle} style={{ color: titleColor }}>
      {title}
    </p>
    <Carousel
      arrows
      centerMode
      removeArrowOnDeviceType={['mobile', 'tablet']}
      responsive={responsive}
      containerClass={style.carouselContainerClass}
      itemClass={style.carouselItemPadding}
      customRightArrow={<Arrow color={arrowColor} />}
      customLeftArrow={<Arrow left color={arrowColor} />}
    >
      {data.map((item) => (
        <Card key={item} />
      ))}
    </Carousel>
  </div>
);

export default CarouselWithTitle;
