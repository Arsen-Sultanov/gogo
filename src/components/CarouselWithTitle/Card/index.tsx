import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { ReactComponent as Heart } from '../../../assets/icons/heart.svg';
import style from './style.module.scss';

const Card = () => {
  const [isHeartActive, setHeartActive] = useState(false);
  const onClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setHeartActive(!isHeartActive);
  };

  return (
    <Link to="/stories">
      <div className={style.card}>
        <div className={style.cardBlueBox}>
          <button
            type="button"
            className={classNames(style.heart, {
              [style.heartActive]: isHeartActive,
            })}
            onClick={onClick}
          >
            <Heart />
          </button>
        </div>
      </div>
    </Link>
  );
};

export default Card;
