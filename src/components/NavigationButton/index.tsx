/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { cloneElement } from 'react';
import { Callback } from '../../types/common';
import style from './styles.module.scss';

const WHITE_COLOR = '#FFFFFF';
const GRAY = '#8E8E8E';

interface navigationButton {
  label: string;
  icon: React.ReactElement;
  active?: boolean;
  activeColor: string;
  onClick?: Callback;
}

const NavigationButton = ({
  icon,
  label,
  active,
  activeColor,
  onClick,
}: navigationButton) => (
  <button type="button" className={style.navgationButton} onClick={onClick}>
    <div
      className={style.navgationButtonIconWrapper}
      style={
        {
          background: active ? activeColor : WHITE_COLOR,
          '--active-color': activeColor,
        } as { [key: string]: string }
      }
    >
      {cloneElement(
        icon,
        {
          ...icon.props,
          fill: active ? WHITE_COLOR : activeColor,
          viewBox: '0 0 44 44',
        },
        null
      )}
    </div>
    <label
      className={style.navgationButtonLabel}
      style={
        {
          color: active ? activeColor : GRAY,
          '--active-color': activeColor,
        } as { [key: string]: string }
      }
    >
      {label}
    </label>
  </button>
);

export default NavigationButton;
