import React from "react"
import { Container, Row } from "react-bootstrap"
import { Link } from "react-router-dom"

export const renderBaseContainer = (children: JSX.Element, titleText?: string, containerClasses? :string): JSX.Element => {
    const classes = containerClasses ? `${containerClasses} mb-2` : 'mb-2'
    return <Container className={classes}>
            {titleText && <Row><h1 className={'text-center color--quaternary mb-3'}>{titleText}</h1></Row>}
            {children}
    </Container>
}

export const renderLogo = (): JSX.Element => {
    return <span className=''> </span>
}

export const renderLinks = (infoText: string, linkText: string, linkTo: string): JSX.Element => {
    return <Row className='mt-1 text-center'>
        <span className='color--tertiary '>{infoText}
            <Link to={linkTo} className='color--primary cursor'>{linkText}</Link>
        </span>
    </Row>
}
