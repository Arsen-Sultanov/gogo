
import * as yup from 'yup';
import { string } from 'yup/lib/locale';
import { ObjectShape } from "yup/lib/object";
import { AnyObject } from 'yup/lib/types';

type ConfiguredValidationModes = 'required' | 'email' | 'min';

type ErrorMessageMap = {
    [key in ConfiguredValidationModes]?: string;
}

const validationYupBuilder = (mode: ConfiguredValidationModes, yupObject: any, message?: string, additionalConfig?: any): ObjectShape => {
    const initYupObject = yupObject ? yupObject : yup.string();
    switch (mode) {
        case 'required':
            return initYupObject.required(message);
        case 'email':
            return initYupObject.email(message);
        case 'min':
            return initYupObject.min(additionalConfig['min'], message);
    }
}

const validationFieldBuilder = (formikValidationFields: FormikValidationField[]) => {
    const validationFields: ObjectShape = {};
    formikValidationFields.map(field => {
        const { name, errorMessageMap, validationModes, additionalConfig } = field;
        let validation: any = yup.string();
        validationModes.forEach(validationMode => {
            const message = errorMessageMap[validationMode] ? errorMessageMap[validationMode] : '';
            validation = validationYupBuilder(validationMode, validation, message, additionalConfig);
        })
        validationFields[name] = validation;
    })
    return validationFields;
}

export interface FormikValidationField {
    name: string;
    errorMessageMap: ErrorMessageMap
    validationModes: ConfiguredValidationModes[],
    additionalConfig?: any
}


export const formikValidationBuilder = (formikValidationFields: FormikValidationField[]) => 
    yup.object().shape(validationFieldBuilder(formikValidationFields))
