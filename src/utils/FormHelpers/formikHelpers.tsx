import Button from "@restart/ui/esm/Button"
import { ErrorMessage, Field, Formik } from "formik"
import { Row } from "react-bootstrap"

export const renderFormikField = (fieldName: string, label: string, placeHolder: string, type="text", isErrorsEnabled = true): JSX.Element => {
    return <>
        <label className={'color--quaternary px-0 fs-5'}>{label}</label>
        <Field as='input' type={type} className='input--container ml-1' 
            id={fieldName} name={fieldName} placeholder={placeHolder} />
        {isErrorsEnabled && <ErrorMessage name={fieldName} />}</>
}


export const renderSubmitButton = (value: string): JSX.Element => {
    return <Row className='mt-3'>
            <Button type='submit' className={'button-primary text-center fw-bold'}>
                <span className='fw-bold fs-4 login-text'>{value}</span>
            </Button> 
    </Row>
}

export const renderFormikForm = (validation: any, initialValues: {}, 
    handleOnSubmit: (formValues: {}) => Promise<void>, form: () => JSX.Element): JSX.Element => {
        return <Formik
                    validationSchema={validation}
                    initialValues={initialValues}
                    onSubmit={handleOnSubmit}>
                        {form}
                </Formik>
}

